# frc-getLocation

This is an example implementation of the method [getLocation](https://first.wpi.edu/wpilib/allwpilib/docs/release/java/edu/wpi/first/wpilibj/DriverStation.html#getLocation()) provided by the wpilib. In this example the value returned is used to run different commands

# Quick Start

## Configuration

The values in Constants.java should be changed to match the target robot setup. Motor models in the subsystem should also be changed accordingly

## Running

The code can be compiled and run as any other frc java project, using the frc tools for vscode
