// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

public final class Constants {
	public static final class DriveConstants {
		public static final int LEFT_LEADER_ID = 2;
		public static final int LEFT_FOLLOWER_ID = 1;
		public static final int RIGHT_LEADER_ID = 4;
		public static final int RIGHT_FOLLOWER_ID = 3;

		public static final boolean LEFT_INVERTED = false;
		public static final boolean RIGHT_INVERTED = false;
	}
}
