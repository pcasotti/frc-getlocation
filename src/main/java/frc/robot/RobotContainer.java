// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.MoveForward;
import frc.robot.commands.TurnLeft;
import frc.robot.commands.TurnRight;
import frc.robot.subsystems.DriveTrain;

public class RobotContainer {

	private final DriveTrain driveTrain = new DriveTrain();

	public RobotContainer() {
		configureButtonBindings();
	}

	private void configureButtonBindings() {}

	public Command getAutonomousCommand() {
		int location = DriverStation.getInstance().getLocation();
		if (location == 3) {
			return new SequentialCommandGroup(
				new MoveForward(driveTrain, 0.5, 2),
				new TurnLeft(driveTrain, 0.5, 2),
				new MoveForward(driveTrain, 0.5, 2)
			);
		}

		if (location == 1) {
			return new SequentialCommandGroup(
				new MoveForward(driveTrain, 0.5, 2),
				new TurnRight(driveTrain, 0.5, 2),
				new MoveForward(driveTrain, 0.5, 2)
			);
		}

		return new MoveForward(driveTrain, 0.5, 3);
	}
}
