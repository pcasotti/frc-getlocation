// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.DriveTrain;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class MoveForward extends CommandBase {

	private final Timer timer = new Timer();

	private final DriveTrain driveTrain;
	private double speed;
	private double time;

	public MoveForward(DriveTrain driveTrain, double speed, double time) {
		this.driveTrain = driveTrain;
		this.speed = speed;
		this.time = time;

		addRequirements(driveTrain);
	}

	@Override
	public void initialize() {
		timer.reset();
		timer.start();
	}

	@Override
	public void execute() {
		driveTrain.drive(speed, speed);
	}

	@Override
	public void end(boolean interrupted) {
		timer.stop();
		driveTrain.drive(0, 0);
	}

	@Override
	public boolean isFinished() {
		return timer.get() >= time;
	}
}
